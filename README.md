# PPW Week 1
This is my assignment for the first web development course!

**CREDITS**

Parallax scrolling mechanism and template inspired by W3Schools

Text fade animations by ScrollyJS

Images by John Kearney, r/wallpaper , and r/apple

Icons by Flaticon and Cream Technology 
